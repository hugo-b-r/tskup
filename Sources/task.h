/*
    Tskup - a cup <~> tsk file converter
    Licenced under the MIT licence.
*/


#ifndef TASK_H
#define TASK_H

#include <libxml/parser.h>

enum ObservationZoneType { OZLine, OZCylinder };
enum PointType { PTurn, PStart, PEnd };
enum TaskType { TTrt, TTaat };


struct TaskPoint {
    enum PointType point_type;
    char *comment;
    char *name;
    int altitude;
    double latitude;
    double longitude;
    int oz_radius;
    enum ObservationZoneType oz_type;
};

void init_task_point();
void free_task_point();


struct Task {
    struct TaskPoint * points;
    int nb_points;
    int pev_start_window;
    int pev_start_wait_time;
    int fai_finish;
    char * finish_min_height_ref;
    int finish_min_height;
    char * start_open_time;
    char * start_max_height_ref;
    int start_max_height;
    int start_max_speed;
    int start_score_exit;
    int start_requires_arm;
    int aat_min_time;
    enum TaskType type;
};

struct Task new_task();
void free_task(struct Task * t);

// Function to parse every xm attributes of a task and put them in the specified struct
void parse_task_attributes(xmlNodePtr root_node, struct Task * task);

// Function to parse evvery xml attributes of a task's point to a TaskPoint structure
void parse_point_attributes(xmlNodePtr point_node, struct TaskPoint * tp);

void parse_waypoint_attributes(xmlNodePtr wp_node, struct TaskPoint * tp);
void parse_observation_zone_attributes(xmlNodePtr oz_node, struct TaskPoint * tp);

void parse_location_attributes(xmlNodePtr location_node, struct TaskPoint * tp);


#endif

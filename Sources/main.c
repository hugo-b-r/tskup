/*
    Tskup - a cup <~> tsk file converter
    Licenced under the MIT licence.
*/

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "task.h"
#include <math.h>



int file_len(FILE *file) {
    int file_len = 0;
    char c;
    for (c = getc(file); c != EOF; c = getc(file)) {
        file_len += 1;
    }
    return file_len;
}

xmlDocPtr get_xml_parsed(char *file_name)
{
    const xmlDocPtr doc = xmlParseFile(file_name);

    if (doc == NULL )
    {
        fprintf(stderr, "Document %s not parsed successfully. Is it an TSK file ?\n", file_name);
        return NULL;
    }
    return doc;
}

enum FileType { Tsk, Cup };

struct TskupCtx {
    char *cup_file_name;
    char *tsk_file_name;
    FILE *cupf;
    FILE *tskf;
    enum FileType file_to_convert;
    xmlDocPtr tsk_file_xml;
    struct Task task;
};

struct TskupCtx null_init_tskupctx() {
    struct TskupCtx ctx;
    ctx.cup_file_name = NULL;
    ctx.tsk_file_name = NULL;
    ctx.file_to_convert = Tsk;
    ctx.tsk_file_xml = NULL;
    ctx.cupf = NULL;
    ctx.tskf = NULL;
    ctx.task = new_task();
    return ctx;
}


void free_tskupctx(struct TskupCtx t) {
    /* not needed because fclose will do it for you
    if (t.cup_file_name != NULL) free(t.cup_file_name);
    */
    if (t.tsk_file_name != NULL) free(t.tsk_file_name);
    if (t.tsk_file_xml != NULL) free(t.tsk_file_xml);
}


void format_coord(double coord, char * coord_str) {
    // converting to degrees minutes seconds without setting if north or east at the end
    double degrees = 0;
    double dec = modf(coord, &degrees);
    double minutes_d = dec * 60;
    double minutes_i;
    double seconds_d = modf(minutes_d, &minutes_i);
    coord_str[0] = ((int) degrees - (int) degrees % 10) / 10 + '0';
    coord_str[1] = (int) degrees % 10 + '0';
    coord_str[2] = ((int) minutes_i - (int) minutes_i % 10) / 10 + '0';
    coord_str[3] = (int) minutes_i % 10 + '0';
    coord_str[4] = '.';
    double tmp_i;
    for (int i = 0; i < 3; i++) {
        seconds_d *= 10;
        seconds_d = modf(seconds_d, &tmp_i);
        coord_str[i+5] = tmp_i + '0';
    }
}


int main(int argc, char** argv) {
    if (argc  != 5) {
        fprintf(stderr, "Error : Not the good number f arguments: requires 4 !");
        return 1;
    }
    struct TskupCtx ctx = null_init_tskupctx();
    ctx.tsk_file_name = NULL;
    ctx.cup_file_name = NULL;
    for (int i = 1; i < 4; i += 2) {
        if (strcmp(argv[i], "--cup") == 0 || strcmp(argv[i], "--tsk") == 0) {
            if (strcmp(argv[i+1], "--tsk") != 0 && strcmp(argv[i+1], "--cup") != 0) {
                if (strcmp(argv[i], "--cup") == 0) {
                    ctx.cup_file_name = malloc(strlen(argv[i+1]));
                    strcpy(ctx.cup_file_name, argv[i+1]);
                    if (i == 1) {
                        ctx.file_to_convert = Cup;
                    }
                } else {
                    ctx.tsk_file_name = malloc(strlen(argv[i+1]));
                    strcpy(ctx.tsk_file_name, argv[i+1]);
                    if (i == 1) {
                        ctx.file_to_convert = Tsk;
                    }
                }
            } else {
                fprintf(stderr, "Error: you need to give a value to your "
                                "argument n° %d, two arguments types are following", i);
                free_tskupctx(ctx);
                return 1;
            }
        } else {
            fprintf(stderr, "Wrong argument passed, please provide any of"
                            "\"--tsk\" or \"--cup\".");
            free_tskupctx(ctx);
            return 1;
        }
    }
    if (ctx.file_to_convert == Tsk) {
        ctx.tsk_file_xml = get_xml_parsed(ctx.tsk_file_name);
        if (ctx.tsk_file_xml == NULL) {
            free_tskupctx(ctx);
            return 1;
        }

        xmlNodePtr cur = xmlDocGetRootElement(ctx.tsk_file_xml);
        if (cur == NULL) {
            fprintf(stderr, "Failed to get the first node of"
                "the file, maybe it is an empty document");
            free_tskupctx(ctx);
            return 1;

        }
        cur = cur->xmlChildrenNode;
        // Counting points
        while (cur != NULL) {
            if (xmlStrcmp(cur->name, (const xmlChar *) "Point") == 0) {
                ctx.task.nb_points += 1;
            } else {
            }
            cur = cur->next;
        }

        ctx.task.points = malloc(ctx.task.nb_points);
        cur = xmlDocGetRootElement(ctx.tsk_file_xml);
        parse_task_attributes(cur, &ctx.task);
        cur = cur->xmlChildrenNode;
        int i_pt = 0;
        while (cur != NULL) {
            if (xmlStrcmp(cur->name, "Point") == 0) {
                parse_point_attributes(cur, &ctx.task.points[i_pt]);
                xmlNodePtr subcur = cur->xmlChildrenNode;
                while (subcur != NULL) {
                    //should check if multiple waypoint
                    if (xmlStrcmp(subcur->name, "Waypoint") == 0) {
                        parse_waypoint_attributes(subcur, &ctx.task.points[i_pt]);
                        xmlNodePtr subsubcur = subcur->xmlChildrenNode;
                        while (subsubcur != NULL) {
                            if (xmlStrcmp(subsubcur->name, "Location") == 0) {
                                parse_location_attributes(subsubcur, &ctx.task.points[i_pt]);
                            }
                            subsubcur = subsubcur->next;
                        }
                    } else if (xmlStrcmp(subcur->name, "ObservationZone") == 0) {
                        parse_observation_zone_attributes(subcur, &ctx.task.points[i_pt]);
                    }
                    subcur = subcur->next;
                }
                i_pt++;
            }
            cur = cur->next;
        }
        // now writing inside of the cup fle everything we just parsed
        ctx.cupf = fopen(ctx.cup_file_name, "w");
        fprintf(ctx.cupf, "\"name\",\"code\",\"country\",\"lat\",\"lon\",\"elev"
                          "\",\"style\",\"rwdir\",\"rwlen\",\"freq\",\"desc\"\n");
        //printing everything waypoint related
        for (int i = 0; i<ctx.task.nb_points; i++) {
            char lat_str[] = "        N";
            char lon_str[] = "        E";
            format_coord(ctx.task.points[i].latitude, lat_str);
            format_coord(ctx.task.points[i].longitude, lon_str);
            fprintf(ctx.cupf, "%s, %s, world, %s, %s, %d.0m, 1,,,,%s\n", ctx.task.points[i].name, ctx.task.points[i].name,
                lat_str, lon_str, ctx.task.points[i].altitude, ctx.task.points[i].comment);
        }
        fprintf(ctx.cupf, "-----Related Tasks-----\n");
        //fprintf(ctx.cupf, ",");
        for (int i = 0; i < ctx.task.nb_points; i++) {
            fprintf(ctx.cupf, ",%s", ctx.task.points[i].name);
        }
        fprintf(ctx.cupf, "\nOptions,NoStart=%s,TaskTime=%s,WpDis=True\n");
        for (int i = 0; i < ctx.task.nb_points; i++) {
            switch (ctx.task.points[i].oz_type) {
                case OZCylinder:
                    fprintf(ctx.cupf, "ObsZone=%d,Style=1,R1=%dm,A1=360\n", i, ctx.task.points[i].oz_radius);
                break;
                case OZLine:
                    fprintf(ctx.cupf, "ObsZone=%d,Style=1,R1=%dm,A1=180,Line=1\n", i, ctx.task.points[i].oz_radius);
                break;
            }
        }
        fclose(ctx.cupf);
        printf("Converted %s into %s", ctx.tsk_file_name, ctx.cup_file_name);

    } else {
        // only programming tsk to cup as of now
        fprintf(stderr, "This only a tsk to cup file converter !");
    }
    free_tskupctx(ctx);
    return 0;
}

/*
    Tskup - a cup <~> tsk file converter
    Licenced under the MIT licence.
*/

#include "task.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

void init_task_point() {
    struct TaskPoint tp;
    tp.point_type = PTurn;
    tp.comment = NULL;
    tp.name = NULL;
    tp.altitude = 0;
    tp.latitude = 0;
    tp.longitude = 0;
    tp.oz_radius = 0;
    tp.oz_type = OZCylinder;
}
void free_task_point(struct TaskPoint * tp) {
    if (tp->comment == NULL) free(tp->comment);
    if (tp->name == NULL) free(tp->comment);
}

struct Task new_task() {
    struct Task t;
    t.nb_points = 0;
    t.points = NULL;
    t.pev_start_window = 0;
    t.pev_start_wait_time = 0;
    t.fai_finish = 0;
    t.finish_min_height_ref = "AGL";
    t.finish_min_height = 0;
    t.start_open_time = "00:00";
    t.start_max_height_ref = "AGL";
    t.start_max_height = 0;
    t.start_max_speed = 0;
    t.start_score_exit = 0;
    t.start_requires_arm = 0;
    t.aat_min_time = 0;
    t.type = TTaat;
    return t;
}

void free_task(struct Task * t) {
    if (t->points == NULL) free(t->points);
    if (t->finish_min_height_ref == NULL) free(t->finish_min_height_ref);
    if (t->start_open_time == NULL) free(t->start_open_time);
    if (t->start_max_height_ref == NULL) free(t->start_max_height_ref);
}

void parse_task_attributes(xmlNodePtr root_node, struct Task * task) {
    task->pev_start_window = (int) *xmlGetProp(root_node, "pev_start_window");
    task->fai_finish = (int) *xmlGetProp(root_node, "fai_finish");
    task->finish_min_height_ref = (char *) xmlGetProp(root_node, "finish_min_height_ref");
    task->finish_min_height = (int) *xmlGetProp(root_node, "finish_min_height");
    task->start_open_time = (char *) xmlGetProp(root_node, "start_open_time");
    task->start_max_height_ref = (char *) xmlGetProp(root_node, "start_max_height_ref");
    task->start_max_height = (int) *xmlGetProp(root_node, "start_max_height");
    task->start_max_speed = (int) *xmlGetProp(root_node, "start_max_speed");
    task->start_score_exit = (int) *xmlGetProp(root_node, "start_score_exit");
    task->start_requires_arm = (int) *xmlGetProp(root_node, "start_requires_arm");
    task->aat_min_time = (int) *xmlGetProp(root_node, "aat_min_time");
    task->type = (int) *xmlGetProp(root_node, "type");
}


void parse_point_attributes(xmlNodePtr point_node, struct TaskPoint * tp) {
    xmlAttrPtr cur_attr = point_node->properties;
    while (cur_attr != NULL) {
        if (xmlStrcmp(cur_attr->name, "type")) {
            if (xmlStrcmp(cur_attr->children->name, "Start")) {
                tp->point_type = PStart;
            } else if (xmlStrcmp(cur_attr->children->name, "Turn")) {
                tp->point_type = PTurn;
            } else if (xmlStrcmp(cur_attr->children->name, "End")) {
                tp->point_type = PEnd;
            }
        }
        cur_attr = cur_attr->next;
    }
    //parsing the sub nodes `Waypoint` and `ObservationZone`
    xmlNodePtr wp_child = point_node->children;
    while(wp_child != NULL) {
        if (xmlStrcmp(wp_child->name, "Waypoint") == 0) {
            parse_waypoint_attributes(wp_child, tp);
        } else if (xmlStrcmp(wp_child->name, "Waypoint") == 0) {
            parse_observation_zone_attributes(wp_child, tp);
        }
        wp_child = wp_child->next;
    }
}

void parse_waypoint_attributes(xmlNodePtr wp_node, struct TaskPoint * tp) {
    tp->altitude = (int) strtod((char *) xmlGetProp(wp_node, "altitude"), NULL);
    tp->comment = (char *) xmlGetProp(wp_node, "comment");
    tp->name = (char *) xmlGetProp(wp_node, "name");
}

void parse_observation_zone_attributes(xmlNodePtr oz_node, struct TaskPoint * tp) {
    char * oz_t = (char *) xmlGetProp(oz_node, "type");
    if (strcmp(oz_t, "Cylinder") == 0) {
        tp->oz_type = OZCylinder;
    } else {
        tp->oz_type = OZLine;
    }
    //printf("%s\n", (char *) oz_node->name);
    switch (tp->oz_type) {
        case OZCylinder:
            tp->oz_radius = (int) strtod((char *) xmlGetProp(oz_node, "radius"), NULL);
            break;
        case OZLine:
            tp->oz_radius = (int) strtod((char *) xmlGetProp(oz_node, "length"), NULL);
            break;
    }

}

void parse_location_attributes(xmlNodePtr location_node, struct TaskPoint * tp) {
    tp->latitude = strtod((char *) xmlGetProp(location_node, "latitude"), NULL);
    tp->longitude = strtod((char *) xmlGetProp(location_node, "longitude"), NULL);
}